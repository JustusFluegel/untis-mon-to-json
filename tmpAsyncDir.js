const tmp = require("tmp");

exports.asyncDir = (options = {}) => {
  return new Promise((resolve, reject) => {
    tmp.dir(options, (err, dirPath, cleanup) => {
      if (err) reject(err);
      resolve({ name: dirPath, removeCallback: cleanup });
    });
  });
};
