const fs = require("fs");
const xray = require("x-ray")();
const table2json = require("tabletojson");
const ent = require("ent");
const tmp = require("tmp");
const promiseTmp = require("./tmpAsyncDir");
const targz = require("targz");
const promiseTargz = require("./targzDecompress");
const { join } = require("path");


const tarGzToJson = async (path, options) => {
  const tmpobj = await promiseTmp.asyncDir({
    unsafeCleanup: true
  });

  await promiseTargz.asyncDecompress({ src: path, dest: tmpobj.name });

  const dirContents = await fs.promises.readdir(tmpobj.name);

  const fileCheck = async file => {
    const stats = await fs.promises.stat(join(tmpobj.name, file));
    return stats.isDirectory();
  };

  const areDirs = await Promise.all(dirContents.map(file => fileCheck(file)));
  const dirs = dirContents.filter((_, i) => areDirs[i]);
  const files = dirContents.filter((_, i) => !areDirs[i]);

  console.table({ areDirs, dirs, files });

  const HEADER_FILE_NAME = "subst_title.htm";
  const COMBINED_FILE_NAME = "subst_001.htm";

  const STUDENTS_FILES_FOLDER_NAME = "f1";
  const TEACHERS_FILES_FOLDER_NAME = "f2";

  if (
    !(
      dirs.every(
        name =>
          name == STUDENTS_FILES_FOLDER_NAME ||
          name == TEACHERS_FILES_FOLDER_NAME
      ) &&
      files.every(
        name => name == HEADER_FILE_NAME || name == COMBINED_FILE_NAME
      ) &&
      dirs.length >= 1 &&
      files.length == 2
    )
  ) {
    tmpobj.removeCallback();
    throw new Error(
      "Invalid directory structure inside of the .tar.gz archive."
    );
  }
  const f1Files = await fs.promises.readdir(
    `${tmpobj.name}/${STUDENTS_FILES_FOLDER_NAME}`
  );

  let students = [];
  let teachers = [];

  if (dirs.includes(STUDENTS_FILES_FOLDER_NAME)) {
    const perFileSeperatedStudents = await Promise.all(
      f1Files.map(file =>
        readTeacherFile(
          `${tmpobj.name}/${STUDENTS_FILES_FOLDER_NAME}/${file}`,
          options
        )
      )
    );
    students = perFileSeperatedStudents.reduce((acc, cur) => [...acc, ...cur]);
  }

  if (dirs.includes(TEACHERS_FILES_FOLDER_NAME)) {
    const perFileSeperatedTeachers = await Promise.all(
      f1Files.map(file =>
        readStudentsFile(
          `${tmpobj.name}/${TEACHERS_FILES_FOLDER_NAME}/${file}`,
          options
        )
      )
    );
    teachers = perFileSeperatedTeachers.reduce((acc, cur) => [...acc, ...cur]);
  }

  tmpobj.removeCallback();
  return { students, teachers };
};


const readUntisFile = async (
  uri,
  { order, encoding, titleClass, tableClass } = {
    order: [],
    encoding: "latin1",
    titleClass: ".mon_title",
    tableClass: ".mon_list"
  }
) => {
  const fileData = await fs.promises.readFile(uri, encoding);

  const pageTitle = await xray(fileData, titleClass);
  const timestamp =
    pageTitle
      .split(" ")[0]
      .split(".")
      .map(el => (el.length == 1 ? `0${el}` : el))
      .reduce((est, cur) => `${cur}-${est}`) + "T00:00:00.000Z";

  const tableData = await xray(fileData, `${tableClass}@html`);

  let currClass = undefined;

  const dataMap = table2json
    .convert(`<table>${tableData}</table>`, {
      stripHtmlFromCells: false,
      headings: order
    })[0]
    .map(obj => {
      if (Object.keys(obj).length == 1) currClass = obj.hour;
      return {
        class: stripHtml(obj.class == null ? currClass : obj.class),
        timestamp: Object.keys(obj).length == 1 ? null : timestamp,
        hour: stripHtml(obj.hour),
        lesson: stripHtml(obj.lesson),
        vtext: stripHtml(obj.vtext),
        teacher: stripHtml(
          obj.teacher == null
            ? undefined
            : obj.teacher.split("?").find(el => el.includes("<s>"))
        ),
        vteacher: stripHtml(
          obj.teacher == null
            ? undefined
            : obj.teacher.split("?").find(el => !el.includes("<s>"))
        ),
        room: stripHtml(
          obj.teacher == null
            ? undefined
            : obj.room.split("?").find(el => el.includes("<s>"))
        ),
        vroom: stripHtml(
          obj.teacher == null
            ? undefined
            : obj.room.split("?").find(el => !el.includes("<s>"))
        )
      };
    })
    .filter(obj => obj.timestamp != null);
  return dataMap;
};

const readTeacherFile = (uri, options) =>
  readUntisFile(uri, {
    order: ["hour", "teacher", "lesson", "class", "room", "vtext", "dummy"],
    encoding: "latin1",
    titleClass: ".mon_title",
    tableClass: ".mon_list",
    ...(options != null ? options : {})
  });


const readStudentsFile = (uri, options) =>
  readUntisFile(uri, {
    order: ["hour", "teacher", "lesson", "room", "vtext", "dummy"],
    encoding: "latin1",
    titleClass: ".mon_title",
    tableClass: ".mon_list",
    ...(options != null ? options : {})
  });


const stripHtml = html =>
  html == null ? html : ent.decode(html.replace(/<[^>]+>/g, ""));

const createDayStamp = (date = new Date()) =>
  date.toISOString().split("T")[0] + "T00:00:00.000Z";

const getNextDay = (oldDate, days = 1) => {
  const date = new Date();
  date.setDate(oldDate.getDate() + days);
  return date;
};

const SUNDAY = 0;
const SATURDAY = 6;

const isSchoolDay = date =>
  date.getDay() != SUNDAY && date.getDay() != SATURDAY;

const getNextSchoolDay = date => {
  const nextDay = getNextDay(date);
  return isSchoolDay(nextDay) ? nextDay : getNextSchoolDay(nextDay);
};

const getNextSchoolDays = (date = new Date()) => {
  date = date instanceof Date ? date : new Date(date);
  const firstDay = isSchoolDay(date) ? date : getNextSchoolDay(date);
  const secondDay = getNextSchoolDay(firstDay);

  return [firstDay, secondDay].map(d => createDayStamp(d));
};

module.exports.readUntisFile = readUntisFile;
module.exports.readStudentsFile = readStudentsFile;
module.exports.readTeacherFile = readTeacherFile;
module.exports.readUntisTarGzArchive = tarGzToJson;
module.exports.dayStamp = createDayStamp;
module.exports.nextSchoolDays = getNextSchoolDays;
