# untis-mon-to-json

Package for converting untis monitor (html) files into json data

## API

### readUntisTarGzArchive

An method who provides an easy way to convert a directory structure to a js object.

```javascript
const untisMonToJson = require("untis-mon-to-json");

untisMonToJson
  .readUntisTarGzArchive("<path>", { encoding: "latin1" })
  .then(({ students, teachers }) => {
    console.table(students);
    console.table(teachers);
  })
  .catch(err => {
    throw err;
  });
```

The directory structure should be like:

```
- f1
  - subst_001.htm
  - subst_002.htm
  - subst_003.htm
  - ...
- f2
  - subst_001.htm
  - subst_002.htm
  - subst_003.htm
  - ...
- subst_001.htm
- subst_title.htm
```

### readStudentsFile / readTeachersFile

```javascript
const untisMonToJson = require("untis-mon-to-json");

untisMonToJson
  .readStudentsFile("<path>", { encoding: "latin1" })
  .then(result => {
    console.table(result);
  })
  .catch(err => {
    throw err;
  });
```

The students file is one of those in the f2 folder, the teachers one of the f1 folder.

### readUntisFile

Exactly like the one above, you have just to provide an order of the headings under the options parameter as "order"

### dayStamp

Simply returns an Iso stamp of the given date, just without time.

### getNextSchoolDays

Looks for the next days (including the current one) except sundays and saturdays.
