const convert = require("./convert");

// console.log(convert.nextSchoolDays());

// convert.readUntisFilev2(
//   "/home/justus_fluegel/Desktop/Neuer Ordner/v_plan/f1/subst_001.htm",
//   {
//     order: ["hour", "teacher", "lesson", "class", "room", "vtext", "dummy"],
//     encoding: "latin1",
//     titleClass: ".mon_title",
//     tableClass: ".mon_list"
//   }
// );

// convert
//   .readUntisFile(
//     "/home/justus_fluegel/Desktop/Neuer Ordner/v_plan/f1/subst_001.htm",
//     {
//       order: ["hour", "teacher", "lesson", "class", "room", "vtext", "dummy"],
//       encoding: "latin1",
//       titleClass: ".mon_title",
//       tableClass: ".mon_list"
//     }
//   )
//   .then(console.log);

convert
  .readUntisTarGzArchive(
    "/home/justus_fluegel/Desktop/Neuer Ordner/plan.tar.gz"
  )
  .then(result => {
    console.table(result["students"]);
    console.table(result["teachers"]);
  })
  .catch(err => {
    console.error(err);
  });
