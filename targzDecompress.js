const targz = require("targz");

exports.asyncDecompress = options =>
  new Promise((resolve, reject) => {
    targz.decompress(options, err => {
      if (err) reject(err);
      resolve();
    });
  });
